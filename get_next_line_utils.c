#include "get_next_line.h"

int		ft_strlen(const char *s)
{
	int k;

	if (!s)
		return (0);
	k = 0;
	while(*s++)
		k++;
	return (k);
}

char	*ft_strdup(char *s)
{
	char *res;
	int	len_s;

	len_s = ft_strlen(s);
	if (!s)
		return (0);
	res = malloc(sizeof(char) * len_s + 1);
	while (*s)
		*res++ = *s++;
	res[len_s] = 0;
	return (res - len_s);
}

char	*check_n(const char *s)
{
	if (!s)
		return (0);

	while (*s)
	{
		if (*s == '\n')
			return ((char *) s);
	s++;
	}
	return (0);
}

char	*add_str(char *line, char *buf)
{
	int	len_buf;
	int	len_line;
	int	total;
	char *res;

	if (!line && !buf)
		return (0);
	len_buf = ft_strlen(buf);
	len_line = ft_strlen(line);
	total = len_buf + len_line;
	res = malloc(sizeof(char) * total + 1);
	while (len_line--)
		*res++ = *line++;
	while (len_buf--)
		*res++ = *buf++;
	*res = 0;
	return (res - total);
}

char *copy_to_n(char *line, char *buf)
{
	int		len_buf;
	int		i;
	char	*res;

	i = 0;
	len_buf = 0;
	while (*buf != '\n')
		len_buf++;
	res = malloc(sizeof(char) * len_buf + 1);
	while (len_buf--)
	{
		res[i] = buf[i];
		i++;
	}
	res[i] = 0;
	return (res);
}