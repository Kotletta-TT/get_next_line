#include "get_next_line.h"

int main()
{
	char *line;
	int fd = open("test.txt", O_RDONLY);
	get_next_line(fd, &line);
	printf("|%s|\n\n", line);
	free(line);
	get_next_line(fd, &line);
	printf("|%s|\n\n", line);
//	while (get_next_line(fd, &line) > 0)
//	{
//		printf("%s\n", line);
//		free(line);
//	}
//	printf("%s\n", line);
//	free(line);
	return 0;
}
