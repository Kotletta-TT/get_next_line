#include "get_next_line.h"

int	get_next_line(int fd, char **line)
{
	static char	buf[BUFFER_SIZE + 1];
	int	read_b;

	if (!line || BUFFER_SIZE <= 0 || fd < 0)
		return (-1);
	read_b = 1;
	if ((ft_strlen(buf)))
		if ((check_n(buf)))

		*line = ft_strdup(buf);
	else
		**line = 0;
	while (read_b)
	{
		read_b = (int) read(fd, buf, BUFFER_SIZE);
		buf[read_b] = 0;
		if ((check_n(buf)))
		{
			*line = copy_to_n(*line, buf);
			break;
		}
		else
			*line = add_str(*line, buf);
	}

	return (0);
}