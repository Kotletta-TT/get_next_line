#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include <stdio.h>
# include <unistd.h>
# include <stdlib.h>
# include <fcntl.h>

int get_next_line(int fd, char **line);
int ft_strlen(const char *s);
char	*ft_strdup(char *s);
char	*check_n(const char *s);
char	*add_str(char *line, char *buf);
char *copy_to_n(char *line, char *buf);

# ifndef BUFFER_SIZE
#  define BUFFER_SIZE 35
# endif
#endif
